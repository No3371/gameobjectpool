# GameObjectPool
A Unity gameobject pooling solution.

## Usage
Declare a GameObjectPool<T> anywhere as you like, the T must be derived from Monobehaviour.
You have to pass a prefab into the constructor, then the pool will handle the rest for you.

Use these method to use with the pool:
- T GetObjectFromPool (Transform targetParent)
- List<T> GetObjectsFromPool (int count, Transform targetParent)
- void ReturnToPool (params T[] returning)
